import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';
import Header from './components/Header/Header'
import About from './components/About/About'
import User from './components/User/User'
import Home from './components/Home/Home'
import SideBar from './components/SideBar/SideBar'
import styles from './App.css'
import { withRouter } from 'react-router-dom'


class App extends Component {
      constructor(props){
        super(props)
     
        this.getTitle = this.getTitle.bind(this);
 }

 componentWillReceiveProps(nextProps){
        document.title = this.getTitle(nextProps.location.pathname)

 }

 componentDidMount(){
  document.title = this.getTitle(this.props.location.pathname)

 }


 getTitle(val) {
    switch(val){
        case "/about":
            return "About us";
        case "/home":
            return "Github";
        case "/user":
            return "User Page";
        default:
            return "Github"
    }
  }

  render () {
    document.body.style.margin = 0;
    document.body.style.overflow="hidden"
    let routes =
      routes = (
        <Switch>
          <Route path="/About"  component={About} />
          <Route path="/user"   component={User} />
          <Route path="/" exact component={Home} />
          <Route component={Home} />
        </Switch>
      );


    return (
      <div>
          <Header/>
          <div  className={styles.container1}>
              <div className={styles.container}>
                  <div className={styles.content}>
                  {routes}
                  </div>
              </div>
          </div>
      </div>
    );
  }
}


export default  withRouter(App) ;

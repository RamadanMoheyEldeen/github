
import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from  './Header.css'
import { withRouter } from 'react-router-dom'

export class Header extends Component {

    render () {
            return (
               <div className={styles.header}>
                    <div className={styles.logo}>
                    <img className={styles.img} src="https://png.icons8.com/metro/50/000000/github.png"/>
                        <h4>GitHub</h4>
                    </div>
                    <div className={styles.nav} >
                    <ul className={styles.ul}>
                                <li className={this.props.location.pathname == "/home" ? styles.active: styles.li} onClick={()=>{this.props.history.push("/home")}}>{"Home"}</li>
                                <li className={this.props.location.pathname.includes("/user") ? styles.active: styles.li} onClick={()=>{ this.props.history.push('/user')}}>{"User"}</li>
                                <li className={this.props.location.pathname == "/about" ? styles.active: styles.li} onClick={()=>{this.props.history.push("/about")}}>{"About"}</li>
                        </ul>
                    </div>
                </div>
        );
    }
}




export default withRouter( Header);

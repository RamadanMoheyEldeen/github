import React from 'react';
import { configure , shallow , mount }from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Header from './Header'

configure({adapter: new Adapter()} )


describe('Header', () => {

    it("should render Header Component", () => {
        const component = shallow(<Header />);
        expect(component.getElements()).toMatchSnapshot();

    });

    it('render  ul  ', () => {
        const wrapper = shallow(<Header />);
        expect(wrapper.find('.ul')).toBeDefined();

    });

});


import React from 'react';
import { configure , shallow , mount }from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import About from './About'

configure({adapter: new Adapter()});


describe('About', () => {

    it("should render About Component", () => {
        const component = shallow(<About />);
        expect(component.getElements()).toMatchSnapshot();
    });

  });
import React from 'react';
import { configure , shallow  } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import SideBar from './SideBar'

configure({adapter: new Adapter()});

const component = shallow(<SideBar />);

describe('SideBar', () => {

    it("should render SideBar Component", () => {
        expect(component.getElements()).toMatchSnapshot();
    });


    it('render  user ', () => {
        const items = ['one', 'two', 'three'];
        const wrapper = shallow(<SideBar users={items} />);
        expect(wrapper.find('.list')).toBeDefined();
      });

  });
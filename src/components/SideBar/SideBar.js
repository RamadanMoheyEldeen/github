
import React from 'react';
import styles from './SideBar.css'
import { connect } from 'react-redux'
import * as actions from '../../../store/actions/';
import { withRouter } from 'react-router-dom'

 class  SideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            users: []
        }
    }

    componentDidMount(){
        this.props.onFetchUsers()
    }

    componentWillReceiveProps(){
        if(this.state.users.length != this.props.users.length)   
            this.setState({users: this.props.users.slice(0,15)})
    }

    render () {
        console.log("props", this.props)
            return (
               <div className={styles.wrapper}>
                    <div>
                        <ul className={styles.list}>
                          {this.state.users.map((user , index )=> {
                            return <li id="userItem" key={index} onClick={()=>{this.props.onSetActiveUser(user.id), this.props.history.replace( `/user/${user.id}` );
                               }}>{user.login}</li>
                          })}
                         </ul>
                    </div>
                    <div className={styles.loadContainer}>
                        <button id='load' onClick={()=>{this.setState({users: this.state.users.length == 15 ? this.props.users : this.props.users.slice(0,15)})}} >{this.state.users.length == 15 ? "load more": "show less"}</button>
                    </div>
               </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        users: state.userReducer.users,
        activeUser: state.userReducer.activeUser
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchUsers: ( ) => dispatch( actions.getUsers() ),
        onSetActiveUser: (user)=> dispatch(actions.getUser(user))
    };
};


export default withRouter(connect(mapStateToProps , mapDispatchToProps )(SideBar));


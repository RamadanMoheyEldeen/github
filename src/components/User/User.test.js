import React from 'react';
import { configure , shallow , mount }from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import User from './User'

configure({adapter: new Adapter()});


describe('User', () => {

 
    it("should render User Component", () => {
        const wrapper = shallow(<User />);
        expect(wrapper.getElements()).toMatchSnapshot();
    });

});
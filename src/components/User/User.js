
import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from './User.css'
import SideBar from '../../components/SideBar/SideBar'
import { withRouter } from 'react-router-dom'
 
export class User extends Component {
    render () {
        console.log(this.props.activeUser)
            return (
               <div className={styles.container1} > 
                        <div className={styles.sidebar}>
                            <SideBar/>
                        </div>  
                        <div  className={styles.content}>
                            <div className={styles.labelContainer}><label>{"User Info"}</label></div>
                                <div className={styles['avatar-container']}>
                                    <img className={styles.avtar} src={this.props.activeUser.avatar_url}/>  
                                    <table className={styles.infoContainer}>
                                        <tbody>
                                            <tr className={styles.row}><td className={styles.labels}>ID</td><td className={styles.values}>{this.props.activeUser.id}</td></tr>
                                            <tr className={styles.row}><td className={styles.labels}>Name</td><td className={styles.values}>{this.props.activeUser.name}</td></tr>
                                            <tr className={styles.row}><td className={styles.labels}>E-mail</td><td className={styles.values}>{this.props.activeUser.email ? this.props.activeUser.email : "NA"}</td></tr>
                                            <tr className={styles.row}><td className={styles.labels}>Created At</td><td className={styles.values}>{this.props.activeUser.created_at ? this.props.activeUser.created_at.slice(0,10) : ''}</td></tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        activeUser: state.userReducer.activeUser,
    };
};


export default withRouter(connect(mapStateToProps ,null )(User));

import React from 'react';
import { configure , shallow , mount }from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Home from './Home'

configure({adapter: new Adapter()});

describe('Home', () => {

    it("should render Home Component", () => {
        const component = shallow(<Home />);
        expect(component.getElements()).toMatchSnapshot();
    });

  });
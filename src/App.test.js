import React from 'react';
import { configure , shallow  }from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Header from './components/Header/Header'
import App from './App'

configure({adapter: new Adapter()});




describe('App', () => {

    it("should render App Component", () => {
        const component = shallow(<App />);
        expect(component.getElements()).toMatchSnapshot();
    });
    
    
    it("is App  contains header " ,()=>{
        const component = shallow(<App />);
        expect(component.containsMatchingElement(<Header />)).toBeDefined();
    
    })

  });
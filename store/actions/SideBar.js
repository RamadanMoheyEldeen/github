
import * as actionTypes from './actionTypes';
import axios from 'axios';

export const setUsers = ( users ) => {
    return {
        type: actionTypes.SET_USERS,
        users: users
    };
};

export const fetchUsersFailed = () => {
    return {
        type: actionTypes.FETCH_USERS_FAILED
    };
};

export const getUsers = () => {
    return dispatch => {
        axios.get( 'https://api.github.com/users' )
            .then( response => {
               dispatch(setUsers(response.data));
               dispatch(getUser(response.data[0].id))
            } )
            .catch( error => {
                dispatch(fetchUsersFailed());
            } );
    };
};

export const getUser = (id) => {
    return dispatch => {
        axios.get( `https://api.github.com/users/${id}` )
            .then( response => {
               dispatch(setActiveUser(response.data))
            } )
    };
};



export const setActiveUser = ( user ) => {
    return {
        type: actionTypes.SET_ACTIVE_USER,
        activeUser: user
    };
};

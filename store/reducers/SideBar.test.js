import reducer from './SideBar'

describe('SideBar reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
        {
            users: [],
            loading: false,
            activeUser: {}
        }
    )
  })
})

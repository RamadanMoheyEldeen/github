import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    users: [],
    loading: false,
    activeUser: {}
};


const fetchUsersSuccess = ( state, action ) => {
    return updateObject( state, {
        users: action.users,
    } );
};

const fetchUsersFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const setActiveUser = ( state, action ) => {
    return updateObject( state, { activeUser: action.activeUser } );
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.SET_USERS: return fetchUsersSuccess( state , action );
        case actionTypes.FETCH_USERS_FAIL: return fetchUsersFail( state, action );
        case actionTypes.SET_ACTIVE_USER: return setActiveUser( state , action );
        default: return state;
    }
};

export default reducer;